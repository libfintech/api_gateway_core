"use strict";
/**
 * 日志
 * Created by liamjung on 2018/5/30.
 */
const {Logger} = require("../entity/logger");
let globalLogger = new Logger();

class Log {

    static debug(str, ...args) {
        (args && args.length > 0) ? globalLogger.getLogger("debug").debug(str, args) : globalLogger.getLogger("debug").debug(str);
    }

    static info(str, ...args) {
        (args && args.length > 0) ? globalLogger.getLogger("info").info(str, args) : globalLogger.getLogger("info").info(str);
    }

    static warn(str, ...args) {
        (args && args.length > 0) ? globalLogger.getLogger("warn").warn(str, args) : globalLogger.getLogger("warn").warn(str);
    }

    static error(str, ...args) {
        (args && args.length > 0) ? globalLogger.getLogger("error").error(str, args) : globalLogger.getLogger("error").error(str);
    }
}

module.exports = {
    Log
};
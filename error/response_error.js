"use strict";

/**
 * 响应异常
 * 用于在出错时，响应客户端
 * Created by liamjung on 2018/5/22.
 */

class ResponseError extends Error {

    constructor(status, headers, body) {

        super();

        this._status = status;
        this._headers = headers;
        this._body = body;
    }

    /**
     * 返回响应状态
     * @returns {module.ResponseError.status}
     */
    get status() {
        return this._status;
    }

    /**
     * 返回响应头
     * @returns {*}
     */
    get headers() {
        return this._headers;
    }

    /**
     * 返回响应体
     * @returns {*}
     */
    get body() {
        return this._body;
    }
}

module.exports = {
    ResponseError
};
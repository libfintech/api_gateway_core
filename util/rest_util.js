"use strict";

/**
 * rest工具
 * Created by liamjung on 2018/5/21.
 */
function execute(req) {

    //解决301、302重定向问题
    req.followRedirect = false;
    //解决显示图片问题
    req.encoding = null;
    //解决响应数据gzip压缩问题
    req.gzip = true;

    return new Promise((resolve, reject) => {
        require("request")(req, function (err, res) {

            if (err)
                reject(err);
            else {
                let body = res.body;
                const headers = res.headers;

                let contentType = (headers["content-type"] || headers["Content-Type"] || "text/plain");
                contentType = contentType.toLowerCase();

                if (contentType.includes("text") || contentType.includes("json") || contentType.includes("xml")) {
                    try {
                        body = body.toString();
                    } catch (err) {
                        //打印日志，忽略
                        Log.warn(err.message);
                    }
                }

                if (headers['content-encoding'] === 'gzip') {
                    delete headers['content-encoding'];
                    headers['content-length'] = Buffer.byteLength(body, "utf8");
                }
                if (headers['Content-Encoding'] === 'gzip') {
                    delete headers['Content-Encoding'];
                    headers['Content-Length'] = Buffer.byteLength(body, "utf8");
                }

                resolve({
                    //res为request模块的response对象
                    status: res.statusCode,
                    headers: res.headers,
                    body: body
                });
            }
        })
    })
}

/**
 * 执行rest请求
 * @param req
 * @param logger
 * @returns {Promise.<[null,null]>}
 */
async function executeRest(req, logger) {

    let url = req.url;

    logger.info("===== url: " + url);

    if (!req.method)
        req.method = "GET";

    if (!req.headers)
        req.headers = {};

    if (req.method === "POST" ||
        req.method === "PUT" ||
        req.method === "PATCH") {

        //不能设置下面值，否则body中含转义字符，如，" 变为 \"
        // req.json = true;

        if (!req.headers["content-type"]) {
            //设置请求Content - Type
            req.headers["Content-Type"] = "application/json;charset=UTF-8";
            //设置请求content - type，兼容全小写
            req.headers["content-type"] = req.headers["Content-Type"];
        }

        //必须设置Content - Length，因为可能存在篡改请求体内容
        //设置请求Content - Length
        req.headers["Content-Length"] = !req.body ? 0 : Buffer.byteLength(req.body, "utf8");
        //设置请求content - length，兼容全小写
        req.headers["content-length"] = req.headers["Content-Length"];
        logger.debug("===== Content-Length: " + req.headers["Content-Length"]);
    } else {

        if (!req.headers["content-type"]) {
            //设置请求Content - Type
            req.headers["Content-Type"] = "text/plain;charset=UTF-8";
            //设置请求content - type，兼容全小写
            req.headers["content-type"] = "text/plain;charset=UTF-8";
        }
    }

    let resp, err;

    try {
        resp = await execute(req);
    } catch (e) {

        err = e;

        logger.error(err);
    }

    return [resp, err];
}

module.exports = {
    executeRest
};
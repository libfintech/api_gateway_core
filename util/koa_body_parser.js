"use strict";
/**
 * koa请求体解析器
 * 扩展koa-bodyparser，增加multipart/form-data请求体解析
 * Created by liamjung on 2018/10/25.
 */
const parse = require("co-body");
const raw = require("raw-body");
const copy = require("copy-to");

/**
 * @param {Object} opts
 *   - {String} jsonLimit default "1mb"
 *   - {String} formLimit default "56kb"
 *   - {String} encoding default "utf-8"
 *   - {Object} extendTypes
 */
module.exports = function (opts) {
    opts = opts || {};
    let detectJSON = opts.detectJSON;
    let onerror = opts.onerror;

    let enableTypes = opts.enableTypes || ["json", "form"];
    let enableForm = checkEnable(enableTypes, "form");
    let enableJson = checkEnable(enableTypes, "json");
    let enableFile = checkEnable(enableTypes, "file");
    let enableText = checkEnable(enableTypes, "text");

    opts.detectJSON = undefined;
    opts.onerror = undefined;

    // force co-body return raw body
    opts.returnRawBody = true;

    // default json types
    let jsonTypes = [
        "application/json",
        "application/json-patch+json",
        "application/vnd.api+json",
        "application/csp-report",
    ];

    // default form types
    let formTypes = [
        "application/x-www-form-urlencoded",
    ];

    // default file types
    let fileTypes = [
        "multipart/form-data"
    ];

    // default text types
    let textTypes = [
        "text/plain",
    ];

    let jsonOpts = formatOptions(opts, "json");
    let formOpts = formatOptions(opts, "form");
    let fileOpts = formatOptions(opts, "file");
    let textOpts = formatOptions(opts, "text");

    let extendTypes = opts.extendTypes || {};

    extendType(jsonTypes, extendTypes.json);
    extendType(formTypes, extendTypes.form);
    extendType(textTypes, extendTypes.file);
    extendType(textTypes, extendTypes.text);

    return async function bodyParser(ctx, next) {
        if (ctx.request.body !== undefined) return await next();
        if (ctx.disableBodyParser) return await next();
        try {
            const res = await parseBody(ctx);
            ctx.request.body = "parsed" in res ? res.parsed : {};
            if (ctx.request.rawBody === undefined) ctx.request.rawBody = res.raw;
        } catch (err) {
            if (onerror) {
                onerror(err, ctx);
            } else {
                throw err;
            }
        }
        await next();
    };

    async function parseBody(ctx) {
        if (enableJson && ((detectJSON && detectJSON(ctx)) || ctx.request.is(jsonTypes))) {
            return await parse.json(ctx, jsonOpts);
        }
        if (enableForm && ctx.request.is(formTypes)) {
            return await parse.form(ctx, formOpts);
        }
        if (enableFile && ctx.request.is(fileTypes)) {
            fileOpts.limit = fileOpts.limit || "1mb";

            let data = await raw(ctx.req, {
                length: ctx.req.headers["content-length"],
                limit: fileOpts.limit,
                // encoding: fileOpts.encoding
            });

            return opts.returnRawBody ? {parsed: data, raw: data} : data;
        }
        if (enableText && ctx.request.is(textTypes)) {
            return await parse.text(ctx, textOpts) || "";
        }
        return {};
    }
};

function formatOptions(opts, type) {
    let res = {};
    copy(opts).to(res);
    res.limit = opts[type + "Limit"];
    return res;
}

function extendType(original, extend) {
    if (extend) {
        if (!Array.isArray(extend)) {
            extend = [extend];
        }
        extend.forEach(function (extend) {
            original.push(extend);
        });
    }
}

function checkEnable(types, type) {
    return types.includes(type);
}

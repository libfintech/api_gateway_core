"use strict";

/**
 * AES对称加密工具
 * Created by liamjung on 2018/7/19.
 */
const crypto = require("crypto");

/**
 * 创建加密算法
 * @param data
 * @param key
 * @returns {*}
 */
function encode(data, key) {
    const cipher = crypto.createCipher("aes192", key);
    let crypted = cipher.update(data, "utf8", "hex");
    crypted += cipher.final("hex");
    return crypted;
}

/**
 * 创建解密算法
 * @param encrypted
 * @param key
 * @returns {*}
 */
function decode(encrypted, key) {
    const decipher = crypto.createDecipher("aes192", key);
    let decrypted = decipher.update(encrypted, "hex", "utf8");
    try {
        decrypted += decipher.final("utf8");
    } catch (err) {
        //打印日志，忽略
        Log.warn(err.message);

        return;
    }
    return decrypted;
}

module.exports = {
    encode,
    decode
};

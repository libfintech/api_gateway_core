"use strict";
/**
 * 工具
 * Created by liamjung on 2018/5/21.
 */
const appRoot = require("app-root-path");
const fs = require("fs");
const RunningModeEnum = require("../enumeration/running_mode");
const {hget} = require("../util/redis_util");
const appConfig = global.appConfig;

/**
 * 获取公众号配置
 * @param mpId
 * @returns {Promise.<*>}
 */
let getMpConfig = async function (mpId) {

    let config;

    switch (appConfig.runningMode) {

        case RunningModeEnum.STANDALONE:
            //读文件
            let filename = appRoot.path + "/" + appConfig.mpConfigName + ".json";
            let configs = JSON.parse(fs.readFileSync(filename));

            for (let key in  configs) {
                if (!configs.hasOwnProperty(key))
                    continue;

                if (key === mpId) {
                    config = configs[key];
                    config.mpId = mpId;

                    break;
                }
            }

            break;
        case RunningModeEnum.CLUSTER:
            //读redis
            const mpConfigName = appConfig.mpConfigName;

            //读redis
            let jsonStr = await hget(mpConfigName, mpId);

            if (jsonStr) {
                config = JSON.parse(jsonStr);
                config.mpId = mpId;
            }

            break;
    }

    return config;
};

/**
 * 检查匹配度
 * @param target    要匹配的字符串
 * @param template  匹配的模板
 * @returns {boolean}
 */
let matchAction = function (target, template) {

    //处理method为通配（*）的场景
    //字母
    let asteriskColon = "[a-zA-Z]+:";
    //除/外的所有字符
    let asterisk = "[^/]+";
    //所有字符
    let asterisk2 = ".+";

    let reg = template
        .replaceAll("*:", asteriskColon)
        .replaceAll("**", asterisk2)
        .replaceAll("*", asterisk);

    let tag = new RegExp("^" + reg + "$").test(target);

    if (tag) {
        Log.debug("===== target: " + target);
        Log.debug("===== template: " + template);
        Log.debug("===== reg: " + reg);
    }

    return tag;
};

/**
 * 匹配配置
 * @param req       请求
 * @param configs   所有配置
 */
let matchConfig = function (req, configs) {

    let target = req.method + ":" + req.originalPath;

    let temps = [];

    for (let template in configs) {

        if (!configs.hasOwnProperty(template))
            continue;

        if (matchAction(target, template))
            temps.push(template);
    }

    if (temps.length === 0)
        return null;

    if (temps.length === 1)
        return configs[temps[0]];

    //优先匹配更长字符串机制
    let temp = "";
    let tempLength = 0;

    for (let template of temps) {

        let templateLength = template.replaceAll("*", "").length;

        if (tempLength < templateLength) {
            temp = template;
            tempLength = templateLength;
        }
    }

    return configs[temp];
};

/**
 * 全部替换
 * @param searchValue
 * @param replaceValue
 * @returns {String}
 */
String.prototype.replaceAll = function (searchValue, replaceValue) {

    let str = this;

    while (str.includes(searchValue))
        str = str.replace(searchValue, replaceValue);

    return str;
};

/**
 * 检查host格式是否为http(s)://ip|域名(:端口)
 * @param host
 * @returns {boolean}
 */
let checkHost = function (host) {

    if (!host)
        return false;

    return host.startsWith("http://") || host.startsWith("https://");
};

module.exports = {
    getMpConfig,
    matchAction,
    matchConfig,
    checkHost
};

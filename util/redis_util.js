"use strict";
/**
 * redis工具
 * Created by liamjung on 2018/5/21.
 */
const redis = require("redis");
const appConfig = global.appConfig;

let client;

function createClient() {
    if (client) {
        // 存在时，必须先推出，否则冗余
        client.quit();
        client = undefined;
    }

    let options = {};

    if (appConfig.redis.pass)
        options.password = appConfig.redis.pass;

    if (appConfig.redis.db)
        options.db = appConfig.redis.db;

    client = redis.createClient(appConfig.redis.port, appConfig.redis.ip, options);

    client.on("error", error => {
        Log.error(error.toString(), error);

        if (["ETIMEDOUT", "ECONNRESET"].includes(error.code)) {
            // 重连
            createClient();
        }
    });
}

if (appConfig.redis) {
    createClient();
}

let _get = function (key) {
    return new Promise((resolve, reject) => {
        client.get(key, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        })
    });
};

let _set = function (key, value) {
    return new Promise((resolve, reject) => {
        client.set(key, value, (err, ok) => {
            if (err)
                reject(err);
            else
                resolve(ok);
        });
    });
};

let hashGet = function (key, hKey) {
    return new Promise((resolve, reject) => {
        client.hget(key, hKey, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        })
    });
};

let hashGetAll = function (key) {
    return new Promise((resolve, reject) => {
        client.hgetall(key, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        })
    });
};

let hashSet = function (key, hKey, value) {
    return new Promise((resolve, reject) => {
        client.hset(key, hKey, value, (err, ok) => {
            if (err)
                reject(err);
            else
                resolve(ok);
        });
    });
};

let hashDel = function (key, hKey) {
    return new Promise((resolve, reject) => {
        client.hdel(key, hKey, (err, ok) => {
            if (err)
                reject(err);
            else
                resolve(ok);
        });
    });
};

let hashMapSet = function (key, hm) {
    return new Promise((resolve, reject) => {
        client.hmset(key, hm, (err, ok) => {
            if (err)
                reject(err);
            else
                resolve(ok);
        });
    });
};

let _expire = function (key, expireTime) {
    return new Promise((resolve, reject) => {
        client.expire(key, expireTime, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        })
    });
};

let _del = function (key) {
    return new Promise((resolve, reject) => {
        client.del(key, (err, ok) => {
            if (err)
                reject(err);
            else
                resolve(ok);
        });
    });
};

/**
 * 取值
 * @param key
 * @returns {Promise.<*>}
 */
let get = async function (key) {

    let result;

    try {
        result = await _get(key);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return result;
};

/**
 * 设置K/V
 * @param key
 * @param value
 * @returns {Promise.<*>}
 */
let set = async function (key, value) {

    let ok;

    try {
        ok = await _set(key, value);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return ok;
};

/**
 * 取值
 * @param key
 * @param hKey
 * @returns {Promise.<*>}
 */
let hget = async function (key, hKey) {

    let result;

    try {
        result = await hashGet(key, hKey);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return result;
};

/**
 * 获取所有
 * @param key
 * @returns {Promise.<*>}
 */
let hgetall = async function (key) {

    let result;

    try {
        result = await hashGetAll(key);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return result;
};

/**
 * 设置哈希K/V
 * @param key
 * @param hKey
 * @param value
 * @returns {Promise.<*>}
 */
let hset = async function (key, hKey, value) {

    let ok;

    try {
        ok = await hashSet(key, hKey, value);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return ok;
};

/**
 * 删除哈希K/V
 * @param key
 * @param hKey
 * @returns {Promise.<*>}
 */
let hdel = async function (key, hKey) {

    let ok;

    try {
        ok = await hashDel(key, hKey);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return ok;
};

/**
 * 设置哈希map
 * @param key
 * @param hm
 * @returns {Promise.<*>}
 */
let hmset = async function (key, hm) {

    let ok;

    try {
        ok = await hashMapSet(key, hm);
    } catch (err) {
        Log.error(err.toString(), err);
    }

    return ok;
};

/**
 * 设置过期时间，单位：毫秒
 * @param key
 * @param expireTime
 * @returns {Promise.<void>}
 */
let expire = async function (key, expireTime) {

    try {
        await _expire(key, Math.round(expireTime / 1000));
    } catch (err) {
        Log.error(err.toString(), err);
    }
};

/**
 * 删除
 * @param key
 * @returns {Promise.<void>}
 */
let del = async function (key) {

    try {
        await _del(key);
    } catch (err) {
        Log.error(err.toString(), err);
    }
};

/**
 * 关闭
 */
let close = function () {
    client.quit();
};

module.exports = {
    get,
    set,
    hget,
    hgetall,
    hset,
    hdel,
    hmset,
    expire,
    del,
    close
};
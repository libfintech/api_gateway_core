"use strict";

/**
 * hex工具
 * Created by liamjung on 2018/7/26.
 */

/**
 * 随机数hex
 * @returns     {string}
 */
function randomHex() {
    let id = Math.random() * Math.pow(10, 16);
    let data = [];
    writeHexLong(data, 0, id);
    return data.join("");
}

/**
 * @param data  char[]
 * @param pos   int
 * @param v     long
 */
function writeHexLong(data, pos, v) {
    writeHexByte(data, pos + 0, ((v >>> 56) & 0xff));
    writeHexByte(data, pos + 2, ((v >>> 48) & 0xff));
    writeHexByte(data, pos + 4, ((v >>> 40) & 0xff));
    writeHexByte(data, pos + 6, ((v >>> 32) & 0xff));
    writeHexByte(data, pos + 8, ((v >>> 24) & 0xff));
    writeHexByte(data, pos + 10, ((v >>> 16) & 0xff));
    writeHexByte(data, pos + 12, ((v >>> 8) & 0xff));
    writeHexByte(data, pos + 14, (v & 0xff));
}

/**
 * @param data  char[]
 * @param pos   int
 * @param b     byte
 */
function writeHexByte(data, pos, b) {
    data[pos + 0] = HEX_DIGITS[(b >> 4) & 0xf];
    data[pos + 1] = HEX_DIGITS[b & 0xf];
}

const HEX_DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

module.exports = {
    randomHex
};

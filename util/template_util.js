"use strict";
/**
 * 模板工具
 * Created by liamjung on 2018/6/29.
 */
const appRoot = require("app-root-path");
const fs = require("fs");

const MSG_TEMPS = {};

/**
 * 读取普通模板
 * @param path
 * @returns {*}
 */
function getMsgTemp(path) {

    let msgTemp = MSG_TEMPS[path];

    if (!msgTemp) {

        try {
            //同步读取文件
            msgTemp = fs.readFileSync(path, "utf8");
        } catch (err) {
            console.log(err);
            //读取文件失败时，打印日志，抛出异常
            Log.warn("Read msg temp file (name: " + path + ") error");

            throw err;
        }

        MSG_TEMPS[path] = msgTemp;
    }

    return msgTemp;
}

/**
 * 获取消息
 * @param msgTempPath
 * @param data
 * @returns {*}
 */
function getMsg(msgTempPath, data) {

    let content;

    try {
        content = getMsgTemp(appRoot.path + msgTempPath);

        if (data) {
            for (let d of data) {
                let key = "${" + d.name + "}";

                //在util中定义replaceAll方法
                content = content.replaceAll(key, d.value);
            }
        }
    } catch (err) {
        //忽略
    }

    return content;
}

module.exports = {
    getMsg
};
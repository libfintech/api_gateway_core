"use strict";
/**
 * 微信api工具
 * Created by liamjung on 2018/7/5.
 */
const clone = require("clone");
const {hset} = require("./redis_util");
const RunningModeEnum = require("../enumeration/running_mode");
const urlencode = require("urlencode");
const WechatAPI = require("co-wechat-api");
WechatAPI.prototype.oauth2buildAuthorizationUrl = function (redirectURI, scope, state) {
    return "https://open.weixin.qq.com/connect/oauth2/authorize?" +
        "appid=" + this.appid + "&" +
        "redirect_uri=" + urlencode.encode(redirectURI) + "&" +
        "response_type=code&" +
        "scope=" + scope + "&" +
        "state=" + state.trim() + "#wechat_redirect"
};
WechatAPI.prototype.ensureAccessToken = async function () {
    // 调用用户传入的获取token的异步方法，获得token之后使用（并缓存它）。
    let token = await this.getToken();
    let accessToken;
    if (token && (accessToken = new WechatAPI.AccessToken(token.accessToken, token.expireTime)).isValid()) {
        this.logger.debug("current access token: " + JSON.stringify(accessToken));

        return accessToken;
    } else if (this.tokenFromCustom) {
        let err = new Error('accessToken Error');
        err.name = 'WeChatAPIError';
        err.code = 40001;
        throw err;
    }

    this.logger.debug("current access token is invalid, " + Date.now() + ", " + JSON.stringify(token));
    accessToken = await this.getAccessToken();
    this.logger.debug("new access token: " + JSON.stringify(accessToken));

    //fix
    return accessToken;
};

const WECHAT_APIS = {};

/**
 * 获取微信api实例，用于调用微信api
 * @param mpConfig
 * @returns {Promise.<*>}
 */
async function getWechatApi(mpConfig) {

    let mpId = mpConfig.mpId;

    let wechatApi = WECHAT_APIS[mpId];

    if (!wechatApi) {
        //微信的appId、secret一旦确定不会改变，所以wechatApi采用缓存机制
        wechatApi = new WechatAPI(mpConfig.appId, mpConfig.secret);

        WECHAT_APIS[mpId] = wechatApi;
    }

    if (appConfig.runningMode === RunningModeEnum.CLUSTER) {
        wechatApi.getToken = async function () {
            return await getToken(mpConfig);
        };
        wechatApi.saveToken = async function (accessToken) {
            await saveToken(mpConfig, accessToken);
        };
    }

    return wechatApi;
}

const TEMP_ACCESS_TOKENS = {};

/**
 *
 * 获取token
 * @param mpConfig
 * @returns {Promise.<void>} WechatAPI.AccessToken
 */
async function getToken(mpConfig) {

    let accessToken;

    if (mpConfig.isTemp)
        accessToken = TEMP_ACCESS_TOKENS[mpConfig.mpId];
    else
        accessToken = mpConfig.accessToken;

    return accessToken
}

/**
 * 保存token
 * @param mpConfig
 * @param accessToken WechatAPI.AccessToken
 * @returns {Promise.<void>}
 */
async function saveToken(mpConfig, accessToken) {

    if (mpConfig.isTemp) {
        //临时保存
        TEMP_ACCESS_TOKENS[mpConfig.mpId] = accessToken;

        return;
    }

    if (accessToken) {
        //用于保存
        accessToken.expireDisplayTime = new Date(accessToken.expireTime).toLocaleString();
    }

    mpConfig.accessToken = accessToken;

    let copyOfMpConfig = clone(mpConfig);
    delete copyOfMpConfig.mpId;

    await hset(appConfig.mpConfigName, mpConfig.mpId, JSON.stringify(copyOfMpConfig));

    if (accessToken) {
        Log.info("Refresh access token for " + mpConfig.mpId + "\t(accessToken: " + accessToken.accessToken + "; expireTime: " + accessToken.expireDisplayTime + ")");
    } else {
        Log.info("Destroy access token for " + mpConfig.mpId);
    }
}

module.exports = {
    getWechatApi
};
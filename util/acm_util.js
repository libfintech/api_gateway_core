"use strict";
/**
 * acm工具
 * Created by liamjung on 2018/5/29.
 */
const ACMClient = require("acm-client");

/**
 * 从阿里云acm拉取配置
 * @param config
 */
module.exports = function* (config) {

    const client = new ACMClient(config);
    client.on("error", function (err) {
        //打印日志，忽略
        Log.error("acm error", err);
    });

    //监听数据更新
    client.subscribe({dataId: config.dataId, group: config.group}, content => eval("global.appConfig = " + content));

    //主动拉取配置
    eval("global.appConfig = " + (yield client.getConfig(config.dataId, config.group)));

    return global.appConfig;
};
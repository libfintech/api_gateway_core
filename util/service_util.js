"use strict";
/**
 * 服务工具
 * Created by liamjung on 2018/5/21.
 */
const {hget} = require("../util/redis_util");
const {hset} = require("../util/redis_util");
const REGISTRY_KEY = "service:registry";

/**
 * 发现服务
 */
let host = async function (serviceId) {

    if (!serviceId || serviceId === "")
        return;

    let h = await hget(REGISTRY_KEY, serviceId);

    if (!h)
        return;

    return JSON.parse(h).host;
};

/**
 * 发现服务ip
 * @param serviceId
 * @returns {string}
 */
let ip = async function (serviceId) {

    let h = await host(serviceId);

    if (!h)
        return undefined;

    let ip = h.substring(h.indexOf("//") + 2, h.lastIndexOf(":"));

    if (ip === "localhost")
        ip = "127.0.0.1";

    return ip;
};

/**
 * 注册服务
 * @param serviceId
 * @param service
 */
let register = async function (serviceId, service) {
    await hset(REGISTRY_KEY, serviceId, service);
};

module.exports = {
    host,
    ip,
    register
};
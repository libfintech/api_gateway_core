[API网关](https://gitee.com/libfintech/wechat_api_gateway)核心库
========

用于插件开发的预定义类和工具方法

## Content
- 实体（entity）
- 枚举（enumeration）
- 错误（error）
- 日志（log）
- 工具（util）

## Installation

```sh
$ npm install @libfintech/api-gateway-core --save
```

## Example

- 错误
```js
//在运行过程中，抛出ResponseError错误，并未捕获时，终止当前插件的运行，并响应客户端
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");

let status = 403;   //http响应状态
let headers = {};   //http响应头
let body = "..."    //http响应体

throw new ResponseError(status, headers, body);
```

- 日志
```js
//往控制台和日志文件中输出日志
//与pipeline中logger的区别是，日志中不含日志追踪标识（traceId、spanId、parentSpanId等）
const {Log} = require("@libfintech/api-gateway-core/log/log");

Log.debug("...");   //输出debug日志
Log.info("...");    //输出info日志
Log.warn("...");    //输出warn日志
Log.error("...");   //输出error日志
```


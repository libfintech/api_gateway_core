"use strict";
/**
 * 运行模式枚举
 * Created by liamjung on 2018/7/18.
 */
const RunningModeEnum = {
    STANDALONE: "standalone",
    CLUSTER: "cluster"
};

module.exports = RunningModeEnum;
"use strict";

/**
 * 管道
 * Created by liamjung on 2018/5/25.
 */
const {Request} = require("./request");
const {Response} = require("./response");
const {Session} = require("./session");
const {Logger} = require("./logger");

class Pipeline {

    /**
     * 构造方法
     * @param ctx
     */
    constructor(ctx) {

        //koa ctx
        this._ctx = ctx;
        //Request对象
        this._req = new Request(ctx.request);
        //Response对象
        this._resp = new Response(ctx.response);
        //会话
        this._sess = new Session(ctx);
        //日志
        this._logger = new Logger(ctx.request);
    }

    get ctx() {
        return this._ctx;
    }

    get request() {
        return this._req;
    }

    get response() {
        return this._resp;
    }

    get session() {
        return this._sess;
    }

    get logger() {
        return this._logger;
    }
}

module.exports = {Pipeline};
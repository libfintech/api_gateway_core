"use strict";

/**
 * 请求
 * Created by liamjung on 2018/5/25.
 */
const qs = require("querystring");
const urlencode = require("urlencode");
const __url = require("url");
const {checkHost} = require("../util/util");

class Request {

    /**
     * 构造方法
     * @param request   http请求
     */
    constructor(request) {
        //请求url（含querystring）
        // this.uri = ;
        //请求host，格式：http(s)://ip|域名(:端口)
        // this._host = ;
        //原请求路径
        this._originalPath = request.path;
        //请求路径
        this._path = request.path;
        //请求参数
        this.querystring = request.querystring;
        //请求方法
        this.method = request.method;
        //请求头，格式：json
        this.headers = request.headers;
        //请求体，格式：string
        this.body = request.rawBody;
    }

    /**
     * 获取url
     * @returns {*}
     */
    get url() {
        //因url被getter/setter占用，所以字段命名成uri，否则出错
        return this.uri;
    }

    /**
     * 设置url
     * @param url
     */
    set url(url) {
        let urlObj = __url.parse(url);

        this._path = urlObj.pathname;

        if (urlObj.query) {
            //合并
            let q = this.query;
            Object.assign(q, urlencode.parse(urlObj.query));

            this.querystring = urlencode.stringify(q);
        }

        if (!this.headers)
            this.headers = {};

        //往headers注入host
        //格式：ip|域名(:端口)
        this.headers.host = urlObj.host;

        this.uri = urlObj.protocol + "//" + urlObj.host + urlObj.pathname +
            (this.querystring && this.querystring !== "" ? ("?" + this.querystring) : "");
    }

    /**
     * 获取host
     * @returns {*}
     */
    get host() {
        return this._host;
    }

    /**
     * 设置host
     * @param host
     */
    set host(host) {
        if (!host)
            return;

        this._host = host;

        if (checkHost(this._host))
            this.url = this._host + this._path;
    }

    /**
     * 获取originalPath，禁止修改
     * @returns {*}
     */
    get originalPath() {
        return this._originalPath;
    }

    /**
     * 获取path
     * @returns {*}
     */
    get path() {
        return this._path;
    }

    /**
     * 设置path
     * @param path
     */
    set path(path) {
        if (!path)
            return;

        this._path = path;

        if (checkHost(this._host))
            this.url = this._host + this._path;
    }

    /**
     * 获取参数
     * @returns {*}
     */
    get query() {
        const str = this.querystring;
        const c = this._querycache = this._querycache || {};
        return c[str] || (c[str] = qs.parse(str));
    }

    /**
     * 设置参数
     * @param q
     */
    set query(q) {

        this.querystring = urlencode.stringify(q);

        let urlObj = __url.parse(this.url);

        this.uri = urlObj.protocol + "//" + urlObj.host + urlObj.pathname +
            (this.querystring && this.querystring !== "" ? ("?" + this.querystring) : "");
    }
}

module.exports = {Request};
"use strict";

/**
 * 响应
 * Created by liamjung on 2018/5/25.
 */
class Response {

    /**
     * 构造方法
     * @param response  http响应
     */
    constructor(response) {
        //响应状态码
        this.status = response.status;
        //响应头，格式：json
        this.headers = response.headers;
        //响应体，格式：string
        this.body = response.body;
    }

    update(response) {

        if (!response) {
            Log.warn("===== resp is null");

            return;
        }

        //设置状态
        this.status = response.status;

        if (response.headers) {
            //设置响应头
            for (let name in response.headers) {
                if (response.headers.hasOwnProperty(name))
                    this.headers[name] = response.headers[name];
            }
        }

        //设置响应体
        this.body = response.body;
    };
}

module.exports = {Response};
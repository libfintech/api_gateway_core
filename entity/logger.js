"use strict";
/**
 * 日志
 * Created by liamjung on 2018/7/10.
 */
const logRootPath = global.appConfig.log.rootPath;

const consoleLayout = {
    type: "pattern",
    //https://github.com/log4js-node/log4js-node/blob/master/lib/layouts.js
    pattern: "%[%d %p [%X{traceId},%X{spanId},%X{parentSpanId}] -%] %m"
};
const outLayout = {
    type: "pattern",
    pattern: "%d %p [%X{traceId},%X{spanId},%X{parentSpanId}] -% %m"
};

const log4js = require("log4js");
log4js.configure({
    appenders: {
        console: {
            type: "console",
            layout: consoleLayout
        },
        "debug-out": {
            type: "file",
            filename: logRootPath + "/logs/debug/debug.log",
            //10MB
            maxLogSize: 10 * 1024 * 1024,
            backups: 10,
            layout: outLayout
        },
        "info-out": {
            type: "file",
            filename: logRootPath + "/logs/info/info.log",
            maxLogSize: 10 * 1024 * 1024,
            backups: 10,
            layout: outLayout
        },
        "warn-out": {
            type: "file",
            filename: logRootPath + "/logs/warn/warn.log",
            maxLogSize: 10 * 1024 * 1024,
            backups: 10,
            layout: outLayout
        },
        "error-out": {
            type: "file",
            filename: logRootPath + "/logs/error/error.log",
            maxLogSize: 10 * 1024 * 1024,
            backups: 10,
            layout: outLayout
        }
    },
    categories: {
        default: {
            appenders: [
                "console"
            ],
            level: "all"
        },
        debug: {
            appenders: [
                "debug-out"
            ],
            level: "all"
        },
        info: {
            appenders: [
                "console",
                "info-out"
            ],
            level: "all"
        },
        warn: {
            appenders: [
                "console",
                "warn-out"
            ],
            level: "all"
        },
        error: {
            appenders: [
                "console",
                "error-out"
            ],
            level: "all"
        }
    }
});
const TRACE_ID_KEY = global.appConfig.log.traceIdKey;
const SPAN_ID_KEY = global.appConfig.log.spanIdKey;
const PARENT_SPAN_ID_KEY = global.appConfig.log.parentSpanIdKey;

class Logger {

    constructor(request) {
        this.loggers = {};
        if (request) {
            //追踪id
            this.traceId = request.headers[TRACE_ID_KEY];
            this.spanId = request.headers[SPAN_ID_KEY];
            this.parentSpanId = request.headers[PARENT_SPAN_ID_KEY];
        }
    }

    getLogger(cate) {
        let logger = this.loggers[cate];

        if (!logger) {
            //初始化logger
            logger = log4js.getLogger(cate);

            this.loggers[cate] = logger;
        }

        //追踪id
        logger.context.traceId = this.traceId || "";
        logger.context.spanId = this.spanId || "";
        logger.context.parentSpanId = this.parentSpanId || "";

        return logger;
    }

    debug(str, ...args) {
        (args && args.length > 0) ? this.getLogger("debug").debug(str, args) : this.getLogger("debug").debug(str);
    }

    info(str, ...args) {
        (args && args.length > 0) ? this.getLogger("info").info(str, args) : this.getLogger("info").info(str);
    }

    warn(str, ...args) {
        (args && args.length > 0) ? this.getLogger("warn").warn(str, args) : this.getLogger("warn").warn(str);
    }

    error(str, ...args) {
        (args && args.length > 0) ? this.getLogger("error").error(str, args) : this.getLogger("error").error(str);
    }
}

module.exports = {Logger};
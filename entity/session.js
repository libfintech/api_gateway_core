"use strict";

/**
 * 会话
 * Created by liamjung on 2018/6/4.
 */
function getIp(ctx) {

    let ip = ctx.request.headers["x-real-ip"];

    if (!ip) {
        ip = ctx.ip;

        let index = ip.indexOf("::ffff:");

        if (index !== -1) {
            ip = ip.substring(index + 7);
        } else if (ip === "::1") {
            ip = "127.0.0.1";
        }
    }

    return ip;
}

class Session {

    /**
     * 构造方法
     * @param ctx
     */
    constructor(ctx) {
        //TODO 待扩展

        //客户端信息
        this._client = {
            ip: getIp(ctx)
        };
        //服务端会话信息
        // this._server;
    }

    get client() {
        return this._client || {};
    }

    get server() {
        return this._server || {};
    }

    set server(server) {
        this._server = server;
    }
}

module.exports = {Session};
"use strict";
/**
 * 账户
 * Created by liamjung on 2018/5/21.
 */
const {host} = require("../util/service_util");
const {executeRest} = require("../util/rest_util");
const {ResponseError} = require("../error/response_error");
const appConfig = global.appConfig;
const util = require("../util/util");
const WXBizDataCrypt = require("./WXBizDataCrypt");
const aesUtil = require("../util/aes_util");
const uuidv4 = require("uuid/v4");
//初始化aes密钥
// const aesKey = "072e6bc7-acd4-45dc-a821-6f139c48f161";
const aesKey = uuidv4();

const sessionIdKeyName = appConfig.session.sessionIdKeyName;
const setCookieKeyName = appConfig.session.setCookieKeyName;
const maxExpireTime = appConfig.session.maxExpireTime;

/**
 * 获取解密数据
 * @param userInfo  格式：{"encryptedData": "...", iv: "..."}
 * @param sessionKey
 * @param appId
 * @param logger
 * @returns {*}
 */
function getDecryptedData(userInfo, sessionKey, appId, logger) {

    if (!userInfo)
        return null;

    let decryptedData;

    //加密数据
    let encryptedData = userInfo["encryptedData"];
    //加密算法的初始向量
    let iv = userInfo["iv"];

    let pc = new WXBizDataCrypt(appId, sessionKey);

    try {
        //解密数据，含unionId、手机号等数据
        decryptedData = pc.decryptData(encryptedData, iv);
    } catch (err) {
        //打印日志，并忽略
        logger.warn(err.message);
    }

    return decryptedData;
}

class Account {

    constructor(pipeline, options) {

        this.request = pipeline.request;
        this.response = pipeline.response;
        this.logger = pipeline.logger;
        this.options = options || {};
    }

    async login(openIdLoginTag) {

        let resp = {};
        let openId = null;

        let loginDto = JSON.parse(this.request.body);

        let mpId = loginDto["mpId"];
        let code = loginDto["code"];
        let config;
        let sessionKey;

        if (mpId) {

            //微信登录，动态改变body
            this.logger.info("mpId: " + mpId);

            config = await util.getMpConfig(mpId);
            //测试代码
            //config = {};
            //config["subjectId"] = "libfintech"

            let body;

            if (code) {
                //通过code登录

                this.logger.info("code: " + code);

                if (!config)
                    return [null, null];

                let wxAccount;

                if (appConfig.debugMode) {
                    this.logger.info("====================== 调试模式 ======================");
                    wxAccount = await this.getWxAccountByDebugMode(mpId, code);
                } else {
                    this.logger.info("====================== 生产模式 ======================");
                    wxAccount = await this.getWxAccount(config, code);
                }

                // //测试代码
                // let wxAccount = {};
                // wxAccount["openid"] = "o_123";
                // wxAccount["unionid"] = "u_123";

                if (!wxAccount) {
                    this.logger.info("##### wechat account: null");

                    resp.status = 401;

                    return [resp, openId];
                }

                openId = wxAccount["openid"];
                let unionId = wxAccount["unionid"];

                this.logger.debug("#####=====##### openId: " + openId + "; mpId: " + mpId);

                let bodyJson = {};

                if (openIdLoginTag) {
                    //openId登录
                    this.logger.info("##### openIdLoginTag: true");

                    bodyJson["mpId"] = mpId;
                    bodyJson["openId"] = wxAccount["openid"];
                    bodyJson["unionId"] = unionId;
                    bodyJson["subjectId"] = config["subjectId"];
                } else {
                    //unionId登录
                    this.logger.info("##### openIdLoginTag: false");

                    let userInfo;

                    if (config.type === "app") {
                        userInfo = wxAccount.userInfo;
                        unionId = userInfo["unionid"];
                    } else {
                        userInfo = loginDto["userInfo"];
                        sessionKey = wxAccount["session_key"];

                        //解密数据
                        let decryptedData = getDecryptedData(userInfo, sessionKey, config.appId, this.logger);

                        if (!decryptedData) {
                            resp.status = 403;
                            resp.headers = {
                                "content-type": "application/json"
                            };
                            //unionId不存在
                            //===需提示用户授权（code：UNIONID_EMPTY）
                            resp.body = "{\"success\": false, \"code\": \"UNIONID_EMPTY\"}";

                            return [resp, openId];
                        }

                        userInfo = decryptedData;
                        unionId = userInfo.unionId;
                    }

                    bodyJson["mpId"] = mpId;
                    bodyJson["openId"] = wxAccount["openid"];
                    bodyJson["unionId"] = unionId;
                    bodyJson["subjectId"] = config["subjectId"];
                    bodyJson["userInfo"] = JSON.stringify(userInfo);
                }

                body = JSON.stringify(bodyJson);
            } else {
                //直接登录
                loginDto["subjectId"] = config["subjectId"];

                body = JSON.stringify(loginDto);
            }

            this.logger.info("===== ***** ##### body: " + body);

            this.request.body = body;
        } else {
            //普通登录
        }

        let [res, err] = await executeRest(this.request, this.logger);

        if (res) {
            //设置状态
            resp.status = res.status;

            if (res.status === 200) {

                //设置header
                resp.headers = getRespHeaders(res, this.logger);

                //设置响应体
                resp.body = res.body;
            } else {

                let respBody = res.body;

                if (code && res.status === 403) {

                    let respBodyJson = JSON.parse(respBody);

                    let data;

                    if (openIdLoginTag) {

                        data = this.request.body;
                    } else {

                        let dataJson = JSON.parse(this.request.body);

                        dataJson.sessionKey = sessionKey;

                        data = JSON.stringify(dataJson);
                    }

                    //===需转到绑定手机号页面（code：UNION_ACCOUNT_EMPTY）

                    //加密
                    respBodyJson.businessKey = aesUtil.encode(data, aesKey);

                    respBody = JSON.stringify(respBodyJson);
                }

                //设置header
                resp.headers = res.headers;

                //设置响应体
                resp.body = respBody;
            }
        }
        else {
            resp.status = 404;
        }

        return [resp, openId];
    }

    /**
     * 获取微信账户信息（调试模式）
     * @param mpId
     * @param code
     * @returns {Promise.<*>}
     */
    async getWxAccountByDebugMode(mpId, code) {

        //TODO 集成调试机制
        let url = await host("wx_gateway") +
            "/wx_gateway/debugging/v1/accounts/wechat?" +
            "mpId=" + mpId + "&" +
            "code=" + code;

        this.logger.info("access token url: " + url);

        let [res, err] = await executeRest({
            url: url,
            headers: {
                Authorization: "e7cb2506-4865-4804-9ae8-7c87eef285b1"
            }
        }, this.logger);

        let wxAccount;

        if (res) {
            if (res.status === 200) {

                let data = JSON.parse(res.body).data;

                this.logger.info("access token body: " + JSON.stringify(data));

                wxAccount = {};
                wxAccount["openid"] = data["openId"];
                wxAccount["unionid"] = data["unionId"];
                wxAccount["session_key"] = data["sessionKey"];
            } else {
                this.logger.warn("access token body: " + res.body);

                //code失效时，返401
                throw new ResponseError(401);
            }
        } else {
            this.logger.error(JSON.stringify(err));
            //404
            throw new ResponseError(404);
        }

        return wxAccount;
    }

    /**
     * 获取微信账户信息
     * @param config
     * @param code
     * @returns {Promise.<*>}
     */
    async getWxAccount(config, code) {

        let url;

        let type = config["type"];

        if (type === "mini_app") {
            //小程序
            url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + config["appId"] +
                "&secret=" + config["secret"] +
                "&js_code=" + code +
                "&grant_type=authorization_code";
        } else if (type === "mp" || type === "app") {
            //公众号或移动应用
            url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + config["appId"] +
                "&secret=" + config["secret"] +
                "&code=" + code +
                "&grant_type=authorization_code";
        } else {
            //404
            throw new ResponseError(404);
        }

        this.logger.info("access token url: " + url);


        let [res, err] = await executeRest({url: url}, this.logger);

        let wxAccount;

        if (res) {
            if (res.status === 200) {

                wxAccount = JSON.parse(res.body);

                if (wxAccount["errcode"]) {
                    this.logger.warn("access token body: " + res.body);

                    //code失效时，返401
                    throw new ResponseError(401);
                } else {
                    this.logger.info("access token body: " + res.body);
                }
            } else {
                this.logger.error(JSON.stringify(res));

                throw new ResponseError(res.status);
            }
        } else {
            this.logger.error(JSON.stringify(err));
            //404
            throw new ResponseError(404);
        }

        if (type === "app") {
            //移动应用
            url = "https://api.weixin.qq.com/sns/userinfo?" +
                "access_token=" + wxAccount["access_token"] + "&" +
                "openid=" + wxAccount["openid"];

            this.logger.info("app user info url: " + url);

            [res, err] = await executeRest({url: url}, this.logger);

            if (res) {
                if (res.status === 200) {
                    let bodyJson = JSON.parse(res.body);

                    if (bodyJson["errcode"]) {
                        this.logger.warn("app user info body: " + res.body);

                        //code失效时，返401
                        throw new ResponseError(401);
                    } else {
                        this.logger.info("app user info body: " + res.body);

                        wxAccount.userInfo = bodyJson;
                    }
                } else {
                    this.logger.error(JSON.stringify(res));

                    throw new ResponseError(res.status);
                }
            } else {
                this.logger.error(JSON.stringify(err));
                //404
                throw new ResponseError(404);
            }
        }

        return wxAccount;
    }
}


/**
 * 获取响应头
 * @param oriResp
 * @param logger
 */
function getRespHeaders(oriResp, logger) {

    let session = JSON.parse(oriResp.body)["data"];

    let sessionId = session["sessionId"];

    logger.info("##### login sessionId: " + sessionId);

    //cookie过期时间
    let date = new Date();
    date.setTime(date.getTime() + maxExpireTime * 1000);

    //设置header
    let headers = oriResp.headers;
    //header中设置会话id
    headers[sessionIdKeyName] = sessionId;
    //cookie中设置会话id
    headers[setCookieKeyName] = sessionIdKeyName + "=" + sessionId + "; Path=/; Expires=" +
        //设置cookie一天后过期
        //TODO 格式：Sat, 26-May-18 11:15:20 GMT
        date.toGMTString();

    return headers;
}

/**
 * 绑定手机号
 * @param req
 * @param logger
 * @returns {{}}
 */
async function bindMobile(req, logger) {

    let dto = JSON.parse(req.body);
    let resp = {
        status: 403,
        headers: {
            "content-type": "application/json"
        },
        body: "{\"success\": false, \"message\": \"invalid business key\"}"
    };

    if (!dto.businessKey)
        return resp;

    let decodedData = aesUtil.decode(dto.businessKey, aesKey);

    if (!decodedData)
        return resp;

    let decodedDataJson = JSON.parse(decodedData);

    let sessionKey = decodedDataJson.sessionKey;

    delete decodedDataJson.sessionKey;

    delete dto.businessKey;

    if (dto.userInfo) {

        let config = await util.getMpConfig(decodedDataJson.mpId);

        let decryptedData = getDecryptedData(dto.userInfo, sessionKey, config.appId, logger);

        if (decryptedData)
            dto.mobile = decryptedData.phoneNumber;
        else {
            resp.body = "{\"success\": false, \"message\": \"invalid user info\"}"

            return resp;
        }

        delete dto.userInfo;
    }

    dto = Object.assign(dto, decodedDataJson);

    req.body = JSON.stringify(dto);

    logger.info("===== req body: " + req.body);

    let [res, err] = await executeRest(req, logger);

    resp = {};

    if (res) {
        //设置状态
        resp.status = res.status;

        if (res.status === 200) {

            //设置header
            resp.headers = getRespHeaders(res, logger);

            //设置响应体
            resp.body = res.body;
        } else {

            //设置header
            resp.headers = res.headers;

            //设置响应体
            resp.body = res.body;
        }
    } else {
        resp.status = 404;
    }

    return resp;
}

module.exports = {
    Account,
    getRespHeaders,
    bindMobile
};

